<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/clubs','ClubController@all');
Route::group(['prefix' => 'auth'], function() {
    Route::post('/user', 'UserController@create');
    Route::post('/login', 'AuthController@userLogin');
});


Route::middleware(['ApiMiddleware'])->group(function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::get('/user', 'UserController@all');
        Route::get('/user/{id}','UserController@getUser');
        Route::put('/user/{id}','UserController@update');
    });
});

Route::get('/feed/posts', 'PostController@apiPosts');
Route::get('/feed/posts/{id}', 'PostController@apiSinglePost');
Route::get('/feed/categories','CategoryController@apiCats');
Route::get('/feed/categories/posts/{id}','CategoryController@getPosts');

Route::middleware(['ApiMiddleware'])->group(function() {
    Route::get('/me', 'ProfileController@index');
    Route::post('/me/update-profile-image', 'ProfileController@updateProfileImage');
    Route::get('/me/for-me','ProfileController@follows');
    Route::get('/feed/follow/{postid}', 'ProfileController@followPost');
    Route::get('/feed/unfollow/{postid}', 'ProfileController@unfollowPost');
});
