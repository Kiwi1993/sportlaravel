<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('auth:admin')->group(function () {
    Route::get('/', 'HomeController@index');
    Route::get('/clubs/{id?}','ClubController@index');
    Route::post('/add-club','ClubController@store');
    Route::post('/update-club/{id}','ClubController@update');
    Route::get('/delete-club/{id}','ClubController@destroy');

    Route::get('/categories/{id?}','CategoryController@index');
    Route::post('/add-category','CategoryController@store');
    Route::post('/update-category/{id}','CategoryController@update');
    Route::get('/delete-category/{id}','CategoryController@destroy');

    Route::get('/posts','PostController@index');
    Route::get('/post/add','PostController@create');
    Route::post('/post/add','PostController@store');
    Route::get('/post/update/{id}','PostController@edit');
    Route::post('/post/update/{id}', 'PostController@update');
    Route::get('/post/delete/{id}','PostController@destroy');

});

//Authentication
Route::match(['get','post'], '/auth', 'AuthController@index')->name('login');

Route::get('/users','UserController@index');

Route::get('/logout', function() {
    \Illuminate\Support\Facades\Auth::guard('admin')->logout();
    return redirect('/auth');
})->name('logout');
