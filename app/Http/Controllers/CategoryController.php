<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Posts;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = "")
    {

        $category = '';

        if($id != "")
        {
            $category = Category::find($id) ?? "";
        }

        $categories = Category::all();
        return View('app.category', [
            'categories' => $categories,
            'category'  => $category
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required|unique:clubs,name'
        ]);

        $category = new Category;
        $category->name = $request->name;

        $category->save();

        return redirect('/categories')->with('success','Category Added successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required|unique:clubs,name,'.$id
        ]);

        $category = Category::find($id);
        $category->name = $request->name;

        $category->save();

        return redirect('/categories')->with('success','Category Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::where('id',$id)->delete();

        return redirect('/categories')->with('success','Category Deleted Successfully');
    }

    public function apiCats()
    {
        return response()->json([
            'status'    => true,
            'data'      => Category::all()
        ]);
    }

    public function getPosts(Request $request, $id)
    {
        $category = Category::find($id);

        if (!$category) {
           return response()->json([
             'status'   => false,
             'message'  => 'Category does not exist'
           ], 404);
        }

        $post = Posts::with('postImage')->where('category','LIKE',"%$category->name%");

        if($request->perpage) {
            $post = $post->paginate($request->perpage);
        } else {
            $post = $post->get();
        }

        return response()->json([
           'status' => true,
           'data'   => $post
        ]);
    }
}
