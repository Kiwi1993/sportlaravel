<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Utility\Utility;
use App\Models\Category;
use App\Models\Interest;
use App\Models\PostMedia;
use App\Models\Posts;
use App\Models\Reading;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::with('PostImage')->get();

        return View('app.posts',[
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'categories'    => Category::all(),
        ];

        return View('app.addPost',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Posts;
        $request->validate([
            'title'     => 'required',
            'postbody'   => 'required',
            'categories'=> 'required',
        ]);

        $post->title = $request->title;
        $post->content = $request->postbody;
        $post->category = implode(",", $request->categories);
        $post->author = "";
        $post->tags = $request->tags;
        $post->slug = Str::slug($request->title." ".time(), '-');
        $post->published = 1;

        $post->save();

        $image = (new Utility)->upload($request->file('file')->getRealPath());

        $media = new PostMedia;

        $media->postid = $post->id;
        $media->image = $image;

        $media->save();

        return redirect('/post/add')->with('success','Post Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     */
    public function edit($id)
    {
        $post = Posts::find($id);

        if(!$post) abort(404);

        return View('app.updatePost',[
            'post'  => $post,
            'categories'    => Category::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'     => 'required',
            'postbody'   => 'required',
            'categories'=> 'required',
        ]);

        $post = Posts::find($id);

        $post->title = $request->title;
        $post->content = $request->postbody;
        $post->category = implode(",", $request->categories);
        $post->author = "";
        $post->tags = $request->tags;
        $post->slug = Str::slug($request->title." ".time(), '-');

        if($request->hasFile('file')) {
            $image = (new Utility)->upload($request->file('file')->getRealPath());
            $media = new PostMedia;

            $media->postid = $post->id;
            $media->image = $image;
            $media->save();
        }

        $post->save();

        return redirect('/post/update/'.$id)->with('success','Post Created Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Posts::where('id',$id)->delete();
        return redirect('/posts')->with('success','Post Deleted Successfully');
    }

    public function apiPosts(Request $request) {


        if($request->perpage) {
            $posts = Posts::with('PostImage')->paginate($request->perpage);
        } else {
            $posts = Posts::with('PostImage')->get();
        }


        return response()->json([
            'status' => true,
            'data' => $posts
        ]);
    }

    public function apiSinglePost(Request $request, $id) {

        $post = Posts::with('postImage')->find($id);

        if(!$post) {
            return response()->json([
                'status'    => false,
                'message'   => 'Post could not be found'
            ], 404);
        }

        $user = (new Helper)->user($request);
        if ($user) {
            Interest::create([
                'userid'    => $user->userid,
                'interests' => $post->tags.",".$post->category
            ]);

            Reading::create([
               'userid' => $user->userid,
               'postid' => $post->id
            ]);
        }

        return response()->json([
           'status' => true,
           'data'   => $post
        ]);
    }
}
