<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        return View('app.users',[
            'users' => $users
        ]);
    }

    public function all()
    {
        return response()->json([
           'status' => true,
           'data'   => User::all()
        ]);
    }

    public function create(Request $request)
    {
        $user = User::where('email',$request->email)->first();

        if($user) {
            return response()->json([
               'status' => false,
               'message'    => 'Email has already been registered'
            ]);
        }

        $request->validate([
           'name'   => 'required',
           'email'  => 'email|unique:users',
           'club'   => 'required',
           'password' => 'required'
        ]);

        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->football_club = $request->club;

        $user->save();


        return response()->json([
           'status' => true,
           'data'   => $user
        ]);
    }

    public function getUser($id) {

        $user = User::find($id)->first();

        if(!$user) {
            return response()->json([
                'status'    => false,
                'message'   => 'User could not be found'
            ],404);
        }

        return response()->json([
            'status'    => true,
            'data'  => $user
        ]);

    }

    public function update(Request $request, $id) {
        $request->validate([
            'name'   => 'required',
            'email'  => 'email',
            'club'   => 'required',
            'password' => 'required'
        ]);

        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->football_club = $request->club;

        $user->save();

        return response()->json([
            'status'    => true,
            'data'  => $user
        ]);
    }
}
