<?php

namespace App\Http\Controllers;

use App\Models\Clubs;
use Illuminate\Http\Request;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = "")
    {

        $club = '';

        if($id != "")
        {
            $club = Clubs::find($id) ?? "";
        }

        $clubs = Clubs::all();
        return View('app.clubs', [
            'clubs' => $clubs,
            'club'  => $club
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required|unique:clubs,name'
        ]);

        $club = new Clubs;
        $club->name = $request->name;

        $club->save();

        return redirect('/clubs')->with('success','Club Added successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required|unique:clubs,name,'.$id
        ]);

        $club = Clubs::find($id);
        $club->name = $request->name;

        $club->save();

        return redirect('/clubs')->with('success','Club Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Clubs::where('id',$id)->delete();

        return redirect('/clubs')->with('success','Club Deleted Successfully');
    }

    public function all()
    {
        return response()->json([
            'status' => true,
            'data' => Clubs::all()
        ]);
    }
}
