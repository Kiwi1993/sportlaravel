<?php

namespace App\Http\Controllers;

use App\Models\Token;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function index(Request $request) {
        if($request->input()) {
            $request->validate([
                'username'  => 'required',
                'password'  => 'required'
            ]);

            if(Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password])) {
                return redirect()->intended('/');
            }

            return redirect(route('login'))->with('error','Invalid Username/Password');
        }
       return View('auth.login');
    }

    public function userLogin(Request $request) {
        $request->validate([
           'email'      => 'required',
           'password'   => 'required'
        ]);

        $user = User::where('email',$request->email)->first();

        if ($user && Hash::check($request->password, $user->password)) {
            $token = Str::random(200);
            $user->login_token = $token;

            Token::where('userid',$user->id)->delete();

            Token::create([
                'apikey'    => $token,
                'userid'    => $user->id
            ]);

            return response()->json([
                'status'    => true,
                'data'      => $user
            ]);
        }

        return response()->json([
           'status' => false,
           'message'  => 'Invalid Username or Password'
        ], 404);
    }
}
