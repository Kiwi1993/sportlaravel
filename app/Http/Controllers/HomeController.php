<?php
namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Clubs;
use App\Models\Posts;
use App\User;

class HomeController extends Controller
{
    public function index() {
        $data = [
            'users'    => User::all()->count(),
            'posts'    => Posts::all()->count(),
            'clubs'    => Clubs::all()->count(),
            'categories' => Category::all()->count()
        ];
        return View('app.dashboard', $data);
    }
}
