<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Utility\Utility;
use App\Models\Followings;
use App\Models\Interest;
use App\Models\Posts;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    private $user;
    public function __construct(Request $request)
    {
        $this->user = (new Helper)->user($request);
    }

    public function index()
    {


        return response()->json([
            'status'    => true,
            'data'      => $this->user->user
        ]);
    }

    public function updateProfileImage(Request $request)
    {
        $allowed = ['jpg','jpeg','png'];
        if ($request->hasFile('image') && in_array($request->file('image')->extension(), $allowed)) {


            //Search for the user
            $user = User::find($this->user->user->id)->first();

            if(!$user) {
                return response()->json([
                    'status'    => false,
                    'message'   => 'Something went wrong, we couldn\'t find the user'
                ], 404);
            }

            //Upload Image to cloudinary and get return url
            $image = (new Utility)->upload($request->file('image')->getRealPath());

            $user->profile_image = $image;

            $user->save();

            return response()->json([
               'status' => true,
               'data'   => $user
            ]);

        }

        return response()->json([
            'status'    => false,
            'message'   => 'Please upload a valid image'
        ], 422);
    }

    public function follows()
    {
        $follows = Followings::with('post')->where('userid',$this->user->userid)->get();
        $interests = Interest::where('userid',$this->user->userid)->get();

        $allInterests = "";

        foreach($interests as $interest) {
            $allInterests .= $interest->interests;
        }

        $allInterests = array_unique(explode(",", $allInterests));

        $interests = Posts::where(function($query) use ($allInterests) {
            foreach($allInterests as $item) {
                $query->orWhere('category','LIKE',"%$item%")->orWhere('tags','LIKE',"%$item%");
            }
        })->get();

        return response()->json([
            'status'  => true,
            'data'  => [
                'follows'   => $follows,
                'suggested' => $interests
            ]
        ]);
    }

    public function followPost($id) {
        $post = Posts::find($id);

        if(!$post) {
            return response()->json([
               'status' => false,
               'message' => 'Post does not exist'
            ], 404);
        }

        Followings::create([
            'userid'    => $this->user->userid,
            'postid'    => $id
        ]);

        return response()->json([
           'status' => true,
        ]);
    }

    public function unfollowPost($id) {

        Followings::where('userid', $this->user->userid)->where('postid',$id)->delete();

        return response()->json([
            'status' => true,
        ]);
    }
}
