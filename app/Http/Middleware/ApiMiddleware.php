<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Closure;
use Illuminate\Support\Str;

class ApiMiddleware
{
    public function handle($request, Closure $next)
    {
        $auth = $request->header()['authorization'] ?? false;

        if(!$auth) return response()->json([
           'status' => false,
            'error' => 'You need to be logged in to access this resource'
        ], 401);

        $token = explode(" ",$auth[0]);

        $valid = Token::where('apikey',$token[1])->first();
        if(!$valid)  return response()->json([
            'status' => false,
            'error' => 'Token is invalid'
        ], 401);


        return $next($request);
    }
}
