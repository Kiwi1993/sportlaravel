<?php

namespace App\Http\Utility;

use JD\Cloudder\Facades\Cloudder;

class Utility
{
    public function upload($file)
    {
        $cloudder = Cloudder::upload($file);
        $uploadResult = $cloudder->getResult();
        $url = $uploadResult['url'];

        return $url;
    }
}
