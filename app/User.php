<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $hidden = [
        'remember_token','updated_at','password'
    ];
    protected $casts = [
        'created_at' => 'datetime:Y-F-d h:i a',
    ];
}
