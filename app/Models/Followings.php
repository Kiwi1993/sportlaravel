<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Followings extends Model
{
    protected $guarded = [];

    public function post()
    {
        return $this->belongsTo('App\Models\Posts','postid');
    }
}
