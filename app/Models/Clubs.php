<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clubs extends Model
{
    use SoftDeletes;
    protected $table = "clubs";
    protected $hidden = ['created_at','deleted_at'];
}
