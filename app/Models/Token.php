<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
    protected $table = 'token';

    public function user()
    {
        return $this->belongsTo('App\User','userid');
    }
}
