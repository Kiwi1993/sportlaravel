<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = "posts";
    protected $hidden = ['updated_at','author'];
    protected $casts = [
        'created_at' => 'datetime:Y-m-d h:i a'
    ];

    public function postImage()
    {
        return $this->hasOne('App\Models\PostMedia','postid');
    }
}
