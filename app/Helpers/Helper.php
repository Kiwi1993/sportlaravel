<?php

namespace App\Helpers;

use App\Models\Token;
use Illuminate\Http\Request;

class Helper
{
    public function user(Request $request) {
        $auth = $request->header()['authorization'] ?? false;

        if(!$auth) return false;

        $token = explode(" ",$auth[0]);

        $user = Token::with('user')->where('apikey',$token[1])->first();

        return $user;
    }
}
