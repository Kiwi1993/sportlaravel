@extends('template.app')

@section('title','Create New Post')

@section('page')
<style>
    .bootstrap-tagsinput {
        width: 100% !important;
    }

    .label {
        padding: 0 5px !important;
    }
</style>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="card">
                    <header class="card-header">
                        Create New Post
                    </header>
                    <div class="card-body">
                        <form action="{{ url('/post/add') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                            @endif
                            @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                {{ $error }}
                                @endforeach
                            </div>
                            @endif
                            <div class="form-group row">
                                <div class="col-sm-8" style="border-right: 1px solid #e1e1e1;">
                                    <div class="form-group">
                                        <label for="">Post Title</label>
                                        <input type="text" value="{{ old('title') }}" class="form-control"
                                        name="title">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Post Content</label>
                                        <textarea name="postbody" id="" cols="30" rows="10" class="summernote"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label for="" style="border-bottom: 1px solid #e1e1e1;display: block;">Select Categories</label>
                                        @foreach($categories as $category)
                                        <span style="display: block;">
                                            <label>
                                                <input type="checkbox" name="categories[]" value="{{ $category->name }}"> {{ ucfirst($category->name) }}
                                            </label>
                                        </span>
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <label for="" style="border-bottom: 1px solid #e1e1e1;display: block;">Enter Tags</label>
                                        <input type="text" class="form-control" value="{{ old('tags') }}" name="tags" data-role="tagsinput" />
                                    </div>
                                    <div class="form-group">
                                        <label for="" style="border-bottom: 1px solid #e1e1e1;display: block;">Select Post Image</label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 100%;text-align: left;">
                                                <img
                                                src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=no+image"
                                                alt="" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail"
                                            style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                             <span class="btn btn-white btn-file btn-sm">
                                                 <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select Featured Image</span>
                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                 <input type="file" name="file" class="default" accept="image/*"/>
                                             </span>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="form-group text-right">
                                    <button class="btn btn-success btn-sm" type="submit"><i class="fa fa-save"></i> Publish</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </section>
        </div>
    </div>
</section>
</section>
<script>
    jQuery(document).ready(function(){

        $('.summernote').summernote({
            height: 400,
                focus: true                 // set focus to editable area after initializing summernote
            });
    });
</script>
@endsection
