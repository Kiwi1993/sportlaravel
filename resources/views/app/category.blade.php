@extends('template/app')

@section('title','Categories')

@section('page')

    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="card">
                        <header class="card-header">
                            {{$category != "" ? "Update Category":"Add New Category"}}
                        </header>
                        <div class="card-body">
                            <form action="{{($category != "") ? url('update-category/'.$category->id):action('CategoryController@store')}}" method="post">
                                @csrf
                                @if(session('success'))
                                    <div class="alert alert-success">
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <div class="align-items-center">
                                    <div class="col-auto">
                                        <label class="sr-only" for="inlineFormInput">Name</label>
                                        <input type="text" value="{{ $category->name ?? "" }}" required name="name" class="form-control mb-2" id="inlineFormInput" placeholder="Category Name">
                                        @error('name')
                                        <span class="control-label text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn btn-primary mb-2">Submit</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </section>

                </div>
                <div class="col-sm-12">
                    <section class="card">
                        <header class="card-header">
                            Categories
                        </header>
                        <div class="card-body">
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $category)
                                            <tr>
                                                <td><?=$category->name?></td>
                                                <td>
                                                    <a href="{{ url('categories/'.$category->id) }}" class="btn btn-info">Edit</a>
                                                    <a href="{{ url('delete-category/'.$category->id) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this Category?')">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>

@endsection

