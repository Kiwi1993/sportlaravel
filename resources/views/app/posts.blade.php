@extends('template/app')

@section('title','Posts')

@section('page')

    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-sm-12">
                    <section class="card">
                        <header class="card-header">
                            Posts
                            <span class="tools pull-right">
                                <a href="{{ url('post/add') }}" class="btn btn-success text-white">Create New Post</a>
                            </span>
                        </header>
                        <div class="card-body">
                            <div class="adv-table">
                                @if(session('success'))
                                    <div class="alert alert-success">{{ session('success') }}</div>
                                @endif
                                <table class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>Post Image</th>
                                        <th>Date Created</th>
                                        <th>Post Title</th>
                                        <th>Category</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($posts as $post)
                                        <tr>
                                            <td><img src="{{ $post->postImage->image }}" class="img-thumbnail" alt="" /></td>
                                            <td>{{ $post->created_at }}</td>
                                            <td>{{ $post->title }}</td>
                                            <td>{{ $post->category }}</td>
                                            <td>
                                                <a href="{{ url('/post/update/'.$post->id) }}" class="btn btn-info btn-sm">Update</a>
                                                <a href="{{ url('/post/delete/'.$post->id) }}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this post and its media?')">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>

@endsection

