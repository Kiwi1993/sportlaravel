@extends('template/app')

@section('title','Clubs')

@section('page')

    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="card">
                        <header class="card-header">
                            {{$club != "" ? "Update Club":"Add New Club"}}
                        </header>
                        <div class="card-body">
                            <form action="{{($club != "") ? url('update-club/'.$club->id):action('ClubController@store')}}" method="post">
                                @csrf
                                @if(session('success'))
                                    <div class="alert alert-success">
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <div class="align-items-center">
                                    <div class="col-auto">
                                        <label class="sr-only" for="inlineFormInput">Name</label>
                                        <input type="text" value="{{ $club->name ?? "" }}" required name="name" class="form-control mb-2" id="inlineFormInput" placeholder="Sample FC">
                                        @error('name')
                                        <span class="control-label text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn btn-primary mb-2">Submit</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </section>

                </div>
                <div class="col-sm-12">
                    <section class="card">
                        <header class="card-header">
                            Football Clubs/Teams
                        </header>
                        <div class="card-body">
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>Club Name</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($clubs as $club)
                                            <tr>
                                                <td><?=$club->name?></td>
                                                <td>
                                                    <a href="{{ url('clubs/'.$club->id) }}" class="btn btn-info">Edit</a>
                                                    <a href="{{ url('delete-club/'.$club->id) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this Club?')">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>

@endsection

